Hayden McPhee
CS283: Systems Architecture I
Assignment 1 (G1)
email: hfm24@drexel.edu

Files that should be present: makefile, mcphee_G1.c, mcphee_G1_Crossword.c, hashTable.c

Description: mcphee_G1.c makes use of a dictionary to fill a hash table with the dictionary's words. The hash table is then used to look up words and compare them to a provided argument to see if they are an anagram of that argument. E.g. if the argument is 'foo', the program will find the hash value of 'foo' and search the corresponding index of the hashtable for any words that are anagrams of 'foo'. The mcphee_G1_Crossword.c program is similar, but takes in two additional arguments: a letter and a number. The letter specifies a fixed letter that should be in a spot in any anagrams being searched, while the number specifies that position. E.g. the word 'foo' with letter 'f' and number '3' will search for any anagrams who have 'f' in their third spot and return only those. The fixed number starts at 1 and not 0 when specifying the spot in the string.

To run: The program should be able to run using the makefile. The argument to mcphee_G1.c is provided to the makefile through the WORD environment variable. For example, to use makefile to find anagrams of foo, type:
	WORD=foo make anagram

for the crossword version, the two additional arguments are LETTER and PLACE, and the make command is 'scrabble'. E.g.:
	WORD=foo LETTER=f PLACE=3 make scrabble
