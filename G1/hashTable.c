#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Linked list node used for multiple entries to the same hash value in the hash table
struct node_t {
	char* val;
	struct node_t* next;
};

// Struct for the hash table. Uses and array of linked lists for its table, where hash values correspond to indices.
struct hashTable_t {
	int size;
	struct node_t** table;
};

// Initializes a hash table with the specified size
struct hashTable_t* makeHashTable(int size) {
	if (size < 1)
		return NULL;

	struct hashTable_t* newHash = malloc(sizeof(struct hashTable_t));
	newHash->size = size;
	newHash->table = malloc(sizeof(struct node_t*)*newHash->size);

	int i;
	for (i = 0; i < newHash->size; i++) {
		newHash->table[i] = NULL;
	}
	
	return newHash;
}

// Returns the computed hashvalue for the string
unsigned int hash(struct hashTable_t* hashTable, char* str) {
	unsigned int hashval = 0;

	int i;
	for (i = 0; i < strlen(str); i++) {
		// For each character, get it's lower case ascii value and subtract 96 (since 'a' is 97, the hashvalue for 'a' is 1, 'b' is 2, etc.)
		hashval += (int) tolower(str[i]) - 96;
	}

	return hashval % hashTable->size;
}

// Returns the node that the string is located in, if it exists in the hash table. Returns NULL if the string is not found.
struct node_t* lookupStr(struct hashTable_t* hashTable, char* str) {
	struct node_t* node;
	unsigned int hashVal = hash(hashTable, str);
	
	for (node = hashTable->table[hashVal]; node != NULL; node = node->next) {
		if (strcmp(str, node->val) == 0)
			return node;
	}
	
	return NULL;
}

// Inserts the specified string into the hash table
int addStr(struct hashTable_t* hashTable, char* str) {
	struct node_t* newList = malloc(sizeof(struct node_t));
	struct node_t* currentList;
	unsigned int hashVal = hash(hashTable, str);

	currentList = lookupStr(hashTable, str);
	if (currentList != NULL)
		return 2;

	newList->val = strdup(str);
	newList->next = hashTable->table[hashVal];
	hashTable->table[hashVal] = newList;

	return 0;
}


// Deconstructs the hash table, freeing up the memory.
void freeTable(struct hashTable_t* hashTable) {
	int i;
	struct node_t* node;
	struct node_t* temp;

	for (i = 0; i < hashTable->size; i++) {
		node = hashTable->table[i];
		while (node != NULL) {
			temp = node;
			node = node->next;
			free(temp->val);
			free(temp);
		}
	}

	free(hashTable->table);
	free(hashTable);
}
