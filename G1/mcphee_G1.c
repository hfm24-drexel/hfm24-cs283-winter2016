#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashTable.c"


// Sorts all characters in the string in alphabetical order
char* sortStr(char* str) {
	char* newStr = malloc(sizeof(char)*(strlen(str)+1));
	char ch;
	int alpha[26] = {0}, strLength, c, i, x;
	
	strLength = strlen(str);
	for (c = 0; c < strLength; c++) {
		ch = tolower(str[c]) - 'a';
		alpha[ch]++;	
	}

	i = 0;

	for (ch = 'a'; ch <= 'z'; ch++) {
		x = ch - 'a';
		for (c = 0; c < alpha[x]; c++) {
			newStr[i] = ch;
			i++;
		}
	}

	newStr[i] = '\0';

	return newStr;
}

// Searches for anagrams of the specified string in the stored dictionary. Returns a null array if none are found. Otherwise, the results are returned as an array of strings.
char** findAnagrams(struct hashTable_t* anagramTable, char* str) {
	// Array to hold matching anagrams. Arbitrary length 25, since there isn't likely to be more than 25 matching anagrams.
	char** anagramArray = malloc(sizeof(char*)*25);
	int i = 0;
	
	// Compute the string's hash value for the hash table
	unsigned int hashValue = hash(anagramTable, str);
	struct node_t* list = anagramTable->table[hashValue];
	char* sortedStr = sortStr(str);
	
	// While the list in the hash table's index isn't empty, compare the stored string with the specified string. If they are anagrams, add the stored string to the array.
	while (list != NULL) {
		if (strcmp(sortedStr,sortStr(list->val)) == 0) {
			char* anagram = malloc(sizeof(char)*(strlen(list->val)+1));
			strncpy(anagram, list->val, strlen(list->val)+1);
			if (i > sizeof(anagramArray)/sizeof(char*)) {
				anagramArray = realloc(anagramArray, sizeof(anagramArray)*2);
			}
			anagramArray[i] = anagram;
			i++;
		}
		list = list->next;
	}

	return anagramArray;
}


void main(int argc, char** argv) {
	printf("\n\n");
	struct hashTable_t* anagramTable = makeHashTable(1000);
	
	FILE* dict = fopen("/usr/share/dict/words", "r"); //open the dictionary for read-only access
   	if(dict == NULL) {
		return;
    	}

    	// Read each line of the file, and add it to the anagram hash table
	printf("Filling dictionary...\n");
    	char word[128];
    	while(fgets(word, sizeof(word), dict) != NULL) {
		word[strlen(word)-1] = 0;
		addStr(anagramTable, word);
	}

	char* wordToSearch;
	if (argc > 1) {
		wordToSearch = malloc(sizeof(char)*(strlen(argv[1])+1));
		strncpy(wordToSearch, argv[1], strlen(argv[1])+1);
	
		printf("Searching dictionary for anagrams of %s...\n", wordToSearch);
		char** anagramList = malloc(sizeof(findAnagrams(anagramTable, wordToSearch)));	
		anagramList = findAnagrams(anagramTable, wordToSearch);
		int i;
		if (anagramList[0] == NULL) {
			printf("No anagrams found for %s\n", wordToSearch);
		}
		else {
			printf("Anagrams found:\n");
			for (i = 0; i < sizeof(anagramList)/sizeof(char*); i++) {
				if (anagramList[i] != NULL) {
					printf("%s\n", anagramList[i]);
				}
				free(anagramList[i]);
			}
		}
		free(anagramList);

		
	} else {
		printf("No argument found specifying what word to find anagrams for\n");
	}	
	printf("\n\n");
	
	free(wordToSearch);
	freeTable(anagramTable);
	
}
