#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashTable.c"


// Sorts all characters in the string in alphabetical order
char* sortStr(char* str) {
	char* newStr = malloc(sizeof(char)*(strlen(str)+1));
	char ch;
	int alpha[26] = {0}, strLength, c, i, x;
	
	strLength = strlen(str);
	for (c = 0; c < strLength; c++) {
		ch = tolower(str[c]) - 'a';
		alpha[ch]++;	
	}

	i = 0;

	for (ch = 'a'; ch <= 'z'; ch++) {
		x = ch - 'a';
		for (c = 0; c < alpha[x]; c++) {
			newStr[i] = ch;
			i++;
		}
	}

	newStr[i] = '\0';

	return newStr;
}

struct anagramList_t {
	char* anagram;
	struct anagramList_t* next;
};

char** findAnagrams(struct hashTable_t* anagramTable, char* str, char* fixedLetter, char* fixedPosition) {
	char** anagramArray = malloc(sizeof(char*)*25);
	int i = 0;
	
	unsigned int hashValue = hash(anagramTable, str);
	struct node_t* list = anagramTable->table[hashValue];
	char* sortedStr = sortStr(str);

	while (list != NULL) {
		if (strcmp(sortedStr,sortStr(list->val)) == 0 && list->val[atoi(fixedPosition)] != fixedLetter[0]) {
			char* anagram = malloc(sizeof(char)*(strlen(list->val)+1));
			strncpy(anagram, list->val, strlen(list->val)+1);
			if (i > sizeof(anagramArray)/sizeof(char*)) {
				anagramArray = realloc(anagramArray, sizeof(anagramArray)*2);
			}
			anagramArray[i] = anagram;
			i++;
		}
		list = list->next;
	}

	return anagramArray;
}


void main(int argc, char** argv) {
	printf("\n\n");
	struct hashTable_t* anagramTable = makeHashTable(1000);
	
	FILE* dict = fopen("/usr/share/dict/words", "r"); //open the dictionary for read-only access
   	if(dict == NULL) {
		return;
    	}

    	// Read each line of the file, and add it to the anagram hash table
	printf("Filling dictionary...\n");
    	char word[128];
    	while(fgets(word, sizeof(word), dict) != NULL) {
		word[strlen(word)-1] = 0;
		addStr(anagramTable, word);
	}


	char* wordToSearch;
	char* fixedLetter;
	char* fixedPosition;
	if (argc > 3) {
		wordToSearch = malloc(sizeof(char)*(strlen(argv[1])+1));
		strncpy(wordToSearch, argv[1], strlen(argv[1])+1);
		
		fixedLetter = argv[2];
		fixedPosition = argv[3];
	
		printf("Searching dictionary for anagrams of %s, with %s in position %s...\n", wordToSearch, fixedLetter, fixedPosition);
		char** anagramList = malloc(sizeof(findAnagrams(anagramTable, wordToSearch, fixedLetter, fixedPosition)));	
		anagramList = findAnagrams(anagramTable, wordToSearch, fixedLetter, fixedPosition);
		int i;
		if (anagramList[0] == NULL) {
			printf("No anagrams found for %s\n", wordToSearch);
		}
		else {
			printf("Anagrams found:\n");
			for (i = 0; i < sizeof(anagramList)/sizeof(char*); i++) {
				if (anagramList[i] != NULL) {
					printf("%s\n", anagramList[i]);
				}
				free(anagramList[i]);
			}
		}
		free(anagramList);
		free(wordToSearch);

		
	} else {
		printf("Not enough arguments found. Please make sure the first argument is the word to find an anagram for, the second is the fixed letter of the anagram, and the third is the position of the fixed letter\n");
	}	
	printf("\n\n");

	freeTable(anagramTable);
}
