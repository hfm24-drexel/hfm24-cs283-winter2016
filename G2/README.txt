Hayden McPhee
CS283: Systems Architecture 1
Assignment 2 (G2)
email: hfm24@drexel.edu

Files that should be present: 
	Makefile 
	phil.jpg 
	src/hw3.c 
	src/hw3ex.c 
	src/hw3.h
	lib/placeholder.txt

Description: This program creates a mozaic of thumbnails that look similar to an original image (a photographic mozaic). It takes the original image, splits it up into cells, and finds the average colors of those cells. From there, it compares each cell's average color with the average colors of all provided thumbnails and matches it with the thumbnail with the closest color (shortest euclidean distance between the RGB values). It then stitches together all of the matching thumbnails to create the final image.

To run: 
Use "make" and "make clean" to compile and clean the program, respectively.
When executing, the program takes in 4 arguments. The first three are the original image, the number of columns it should be split into, and the number of rows it should be split into. The fourth argument is really any number of arguments, and is the thumbnails that the program will use. It is recommended to use something like "thumbs/*" to get the thumbnails.

Other Requirements: 
	- All thumbnails must have the same dimensions
	- The original image's width must be a multiple of both the number of columns and the width of the thumbnails, and its height must be a multiple of both the number of rows and the height of the thumbnails
	- An original image example is provided as "phil.jpg" that can be used, and is 1280x960. It works with the example execution provided in the assignment description ("./hw3ex phil.jpg 128 95 thumbs/*", where the "thumbs" directory is in the same location as "hw3ex" and contains the example thumbnail library).
	- No thumbnails are included. The directory provided for the assignment is very big, so you'll have to use your own thumbnail library.
