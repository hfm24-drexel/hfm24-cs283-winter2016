#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include "RSA.h"

// Finds the private key d, given the public key e and c
int keycrack(int e, int c) {
	int a, b, d, m;
	for (a = (int)sqrt(e)-1; a < e; a++) {
		if (e % a == 0) {
			b = e / a;
			break;
		}
	}
	printf("a is %i, b is %i\n", a, b);
	m = (a-1)*(b-1);
	d = modular_inverse(e, m);		
	return d;
}

void main(int argc, char **argv) {
	printf("\n");
	if (argc < 2) {
		printf("Not enough arguments. Argument format: <command> <command arguments>\n");
		return;
	}

	char *command = argv[1];
	if (strcmp(command,"genkey") == 0) {
		if (argc != 4) {
			printf("Incorrect number of arguments. Format should be: ./mcphee_G3 genkey <MPRIME> <NPRIME>\n");
			return;
		}
		int a = atoi(argv[2]);
		int b = atoi(argv[3]);
		int **keys = RSA_gen_keys(a, b);
		printf("Public Key: (e:%i, c:%i), Private Key: (d:%i, c:%i)\n", keys[0][0], keys[0][1], keys[1][0], keys[1][1]);

		// Encryption testing for the keys
		printf("(Encryption test) Please enter a message to encrypt:\n");
		char *line = malloc(100);
		fgets(line, 100, stdin);
		int i = (int) line[0];
		int j = 1;
		while (i != 0) {
			printf("%i\n", endecrypt((int)line[j], keys[0][0], keys[0][1]));
			i = (int) line[j];
			j++;
		}
		unsigned long len = 25;
		char *cipher = malloc(len);
		
		// Decryption testing for the keys
		while ((int) *cipher != 113) {
			printf("(Decryption test) Enter next char cipher value as int, type q to quit:\n");
			getline(&cipher, &len, stdin);
			int cipher_len = strlen(cipher)-1;
			char *cipher_end = malloc(1);
			strncpy(cipher_end, cipher+cipher_len, 1);
			int cipher_int = (int) strtol(cipher, &cipher_end, 10);
			printf("\n%c\n", (char) ((int) pow((double) cipher_int,(double) keys[1][0]) % keys[1][1]));
		}

	} else if (strcmp(command, "keycrack") == 0) {
		if (argc != 4) {
			printf("Incorrect number of arguments. Format should be: ./mcphee_G3 keycrack <E> <C>\n");	
			return;
		}
		int e = atoi(argv[2]);
		int c = atoi(argv[3]);
		int d = keycrack(e, c);
		printf("d was found to be %i\n", d);
	
	} else {
		printf("Command %s not recognized. Valid commands:\n\tgenkey\n\tkeycrack\n", argv[1]);
		return;
	}

	printf("\n");
}
