#include "csapp.h"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>

// Contains the arguments for receive_from_server() to use
typedef struct conn_info_t conn_info_t;
struct conn_info_t {
        int *connfd; // pointer to the file descriptor for a server connection
        int d; // private key d
        int c; // private key c
};

// Listens to a connection with a server asynchronously.
void *receive_from_server(void *conn_info) {
        conn_info_t *c_info = (conn_info_t*) conn_info;
        char messageReceived[1024];
        int received;
        int fd = * c_info->connfd;
        int d = c_info->d;
        int c = c_info->c;
        printf("Listening to server\n");
        while (1) {
                if ((received = read(fd, messageReceived, sizeof(messageReceived))) >= 0) {
                        if (received == 0) {
                                printf("Connection closed by server\n");
                                break;
                        }
                        printf("Message received: %s\n", messageReceived);
                        char decrypted_msg[1024];
                        char *split_msg = strtok(messageReceived, " ");
			
			// Decrypt the message
                        int i;
                        int index = 0;
                        while (split_msg != NULL) {
                                int cipher_len = strlen(split_msg)-1;
                                char *cipher_end = malloc(1);
                                strncpy(cipher_end, split_msg+cipher_len, 1);
                                int cipher_int = (int) strtol(split_msg, cipher_end, 10);
                                char decrypted_char = (char) floor( pow( cipher_int, d % c));
                                index += snprintf(&decrypted_msg[index], 1024, "%d ", split_msg);
                                split_msg = strtok(NULL, " ");
                        }

                        printf("Decrypted message: %s\n", decrypted_msg);
                }

        }
        pthread_exit(0);
	exit();
}

void main(int argc, char** argv) {
	if (argc != 7) {
		printf("Incorrect number of arguments. Format should be: <SERVER> <PORT> <E> <C> <D> <DC>\n");
		return;
	}

	int clientfd;
	struct hostent *hp;
	struct sockaddr_in serveraddr;
	char* host = argv[1]; // Host to connect to
	int port_num = atoi(argv[2]);
	int e = atoi(argv[3]);
	int c = atoi(argv[4]);
	int d = atoi(argv[5]);
	int dc = atoi(argv[6]);

	// Client socket creation
	printf("Creating client socket...\n");
	if ((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error: Unable to create a client socket. Exiting.\n");
		return;
	}
	printf("Client socket created successfully. Client fd: %i\n", clientfd);

	// Finding host
	printf("Getting host by name: %s\n", host);
	if ((hp = gethostbyname(host)) == NULL) {
		printf("Error: Unable to get the host. Exiting.\n");
		return;
	}
	printf("Host found successfully: %s \n", hp->h_name);

	// Filling in the structure
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char*) hp->h_addr_list[0], (char*) &serveraddr.sin_addr.s_addr, hp->h_length);
	serveraddr.sin_port = htons(port_num);

	// Connecting to host
	printf("Connecting to server: %s\n", (char*) &serveraddr.sin_addr.s_addr);
	if (connect(clientfd, (SA*) &serveraddr, sizeof(serveraddr)) < 0) {
		printf("Error: Unable to connect to the server. Exiting.\n");
		return;
	}
	printf("Connected successfully.\n");

	// Setting up to listen asynchronously
	pthread_t receive_thread;
	conn_info_t *c_info = malloc(sizeof(conn_info_t));
	c_info->connfd = &clientfd;
	c_info->d = d;
	c_info->c = dc;
	pthread_create(&receive_thread, NULL, receive_from_server, c_info);

	size_t len = 1024;
	int n;
	char *input = malloc(len);
	printf("Type, or enter .bye to quit\n");
	while (strncmp(input, ".bye", strlen(input)-1) != 0) {
		getline(&input, &len, stdin);

		if (strncmp(input, ".bye", strlen(input)-1) == 0) {
			break;
		}

		// Encrypt the input
		char *encr_msg = malloc(len);
		int i = 0;
		char c_to_encr;
		char input_len = strlen(input)-1;
		int *encr_input = malloc(sizeof(int)*input_len);
		for (i = 0; i < input_len; i++) {
			c_to_encr = input[i];
			int encr_c = endecrypt(c_to_encr, e, c);
			encr_input[i] = encr_c;
		}

		int index = 0;
		for (i = 0; i < input_len; i++) {
			index += snprintf(&encr_msg[index], len, "%d ", encr_input[i]);
		}

		// Send the encrypted input
		printf("Encrypted message: %s\n", encr_msg);
		if ((n = write(clientfd, encr_msg, strlen(encr_msg)+1)) < 0) {
			break;
		}
			
	}

	close(clientfd);
	printf("Connection closed\n");
}
