#include "csapp.h"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <pthread.h>
#include <math.h>
#include "RSA.h"

// Contains arguments for receive_from_client() to use
typedef struct conn_info_t conn_info_t;
struct conn_info_t {
	int *connfd;
	int d;
	int c;
};

// Listens to a client connection asynchronously
void *receive_from_client(void *conn_info) {
	conn_info_t *c_info = (conn_info_t*) conn_info;
	char messageReceived[1024];
	int received;
	int fd = * c_info->connfd;
	int d = c_info->d;
	int c = c_info->c;
	printf("Listening to client\n");
	while (1) {
		if ((received = read(fd, messageReceived, sizeof(messageReceived))) >= 0) {
			if (received == 0) {
				printf("Connection closed by client\n");
				break;
			}
			printf("Message received: %s\n", messageReceived);
			char decrypted_msg[1024];
			char *split_msg = strtok(messageReceived, " ");

			// Decrypt the received message
			int i;
			int index = 0;
			while (split_msg != NULL) {
				int cipher_len = strlen(split_msg)-1;
				char *cipher_end = malloc(1);
				strncpy(cipher_end, split_msg+cipher_len, 1);	
				int cipher_int = (int) strtol(split_msg, cipher_end, 10);
				char decrypted_char = (char) floor( pow( cipher_int, d % c)); 
				index += snprintf(&decrypted_msg[index], 1024, "%d ", split_msg);
				split_msg = strtok(NULL, " ");
			}
	
			printf("Decrypted message: %s\n", decrypted_msg);
		}

	}
	pthread_exit(0);
}

void main(int argc, char** argv) {
	if (argc != 6) {
		printf("Wrong number of arguments. Format should be: <PORT> <E> <C> <D> <DC>\n");
		return;
	}

	int port_num = atoi(argv[1]);
	int e = atoi(argv[2]);
	int c = atoi(argv[3]);
	int d = atoi(argv[4]);
	int dc = atoi(argv[5]);

	int listenfd, optval = 1;
	struct sockaddr_in serveraddr;
	int port_no = strtoimax(argv[1], NULL, 10);
	char hostname[1024];
	hostname[1023] = "\0";
	gethostname(hostname, 1023);
	printf("Hostname: %s\n", hostname);	

	// Listening socket creation
	if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error: Unable to create socket. Exiting.\n");
		return;
	}

	// Eliminates "Address already in use" error from bind()
	if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *) &optval, sizeof(int)) < 0) {
		printf("Unable to set socket REUSEADDR option.\n");
		return;
	}

	// Fill in serveraddr specifications
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short) port_num);
	
	if (bind(listenfd, (SA*)&serveraddr, sizeof(serveraddr)) < 0) {
		printf("Unable to bind port to server socket.\n");
		return;
	}
	
	// Make server a listening one	
	if (listen(listenfd, LISTENQ) < 0) {
		printf("Unable to set server to a listening server.\n");
		return;
	}

	// Main server loop	
	int connfd;
	struct sockaddr_in clientaddr;
	int clientlen;
	struct hostent *hp;
	char *haddrp;
	size_t n;
	char request[1024];
	rio_t rio;
	
	printf("Running main server loop.\n");

	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = accept(listenfd, (SA*) &clientaddr, &clientlen);
		hp = gethostbyaddr((const char*) &clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("\n\nserver connected to %s (%s)\n", hp->h_name, haddrp);

		size_t len = 1024;
		char *input = malloc(len);

		// Set up for listening asynchronously
		pthread_t receive_thread;
		conn_info_t *c_info = malloc(sizeof(struct conn_info_t));
		c_info->connfd = &connfd;
		c_info->d = d;
		c_info->c = dc;
		pthread_create(&receive_thread, NULL, receive_from_client, c_info);

		printf("Type, or enter .bye to quit\n");
		while (strncmp(input, ".bye", strlen(input)-1) != 0) {
			getline(&input, &len, stdin);	

			// Encrypt the input
			char *encr_msg = malloc(len);
			int i = 0;
			char c_to_encr;
			char input_len = strlen(input)-1;
			int *encr_input = malloc(sizeof(int)*input_len);
			for (i = 0; i < input_len; i++) {
				c_to_encr = input[i];
				printf("character to encrypt: %c\n", input[i]);
				int encr_c = endecrypt(c_to_encr, e, c);
				printf("encrypted char: %i\n", encr_c);
				encr_input[i] = encr_c;
			}

			int index = 0;
			for (i = 0; i < input_len; i++) {
				index += snprintf(&encr_msg[index], len, "%d ", encr_input[i]);
			}

			// Send the encrypted input
			printf("Encrypted message: %s\n", encr_msg);
			if ((n = write(connfd, encr_msg, strlen(encr_msg)+1)) < 0) {
				break;
			}
		}
		printf("Connection closed\n");
		close(connfd);
	}
}
