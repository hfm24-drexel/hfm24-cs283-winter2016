Hayden McPhee
CS283: Systems Architecture 1
Assignment 3 (G3)
email: hfm24@drexel.edu

Files that should be present:
	makefile
	McPhee_G3.c
	McPhee_G3_Client.c
	McPhee_G3_Server.c
	RSA.c
	RSA.h
	csapp.c
	csapp.h
	

Description: The overall idea of the program is to handle the use of encryption when communicating between a client and a server. It is divided into multiple parts:
	1. McPhee_G3.c: This portion has two functions: 
		It can generate a private and public key based on two numbers you give it (Mth prime and Nth prime)
		It can crack a key based on the e and c given to it
	2. McPhee_G3_Client.c: This portion creates a client connection with a specified server, and allows you to send encrypted messages throught the connection
	3. McPhee_G3_Server.c: This portion creates a server that waits for a client connection. Once it is set up, it allows you to send encrypted messages through the connection.	

Running: 
	To generate keys, use "MPRIME=<some #> NPRIME=<some #> make genkey". 
		e.g. "MPRIME=10 NPRIME=5 make genkey"
	To crack a key, use "E=<E of the public key> C=<C of the public key> make keycrack". 
		e.g. "E=1337 C=65535 make keycrack"
	To set up a client, use "SERVER=<server name> PORT=<port number> E=<E> C=<C> D=<D> DC=<DC> make client", where E and C are the server's public encryption key E and C, and D and DC are your decryption key D and DC. 
		e.g. "SERVER=tux64-11.cs.drexel.edu PORT=8080 E=1337 C=65535 D=3371 DC=65533 make client"
	To set up a server, use "PORT=<port number> E=<E> C=<C> D=<D> DC=<DC>", where E and C are the client's public encryption key E and C, and D and DC are your encryption key D and DC.
		e.g "PORT=8080 E=1337 C=65535 D=3371 DC=65533 make server"

Cleaning: to clean, simply use "make clean"
