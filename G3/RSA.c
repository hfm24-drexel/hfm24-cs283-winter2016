#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "RSA.h"

int is_prime(int n) {
	int i;
	for (i = 2; i < n; i++) {
		if (n % i == 0) {
			return 0;
		}
	}
	return 1;
}

int is_coprime(int m, int n) {
	int t;
	while (n != 0) {
		t = m;
		m = n;
		n = t % n;
	}	
	if (m == 1) {
		return 1;
	} else {
		return 0;
	}
}

int nth_prime(int n) {
	int prime_count=0, i=1;
	while (prime_count < n) {
		//printf("%i, %i\n", prime_count, i);
		i++;	
		if (is_prime(i) == 1) {
			prime_count++;	
		}
	}
	return i;
}

int RSA_composite_coprime(int m, int c) {
	int e = m * 2;
	while (is_coprime(e, c) == 0 || is_coprime(e, m) == 0) {
		//printf("e: %i\n", e);
		e++;
	}
	return e;

}

int modular_inverse(int m, int n) {
	int x = 2;
	//printf("%i, %i\n", m, n);
	while ((x * m) % n != 1) {
		//printf("%i*%i ---- 1 mod %i\n", x, m, n);
		x++;
	}
	return x;
}

int endecrypt(int cipher, int key, int c) {
	int cipher_l = (long int) cipher;
	int key_l = (long int) key;
	int c_l = (long int) c;
	int result;
	result = (cipher_l * key_l) % c_l;
	return result;
}

int **RSA_gen_keys(int a, int b) {
	printf("a: %i, b: %i\n", a, b);
	int ath_prime = nth_prime(a);
	int bth_prime = nth_prime(b);
	printf("ath_prime: %i, bth_prime: %i\n", ath_prime, bth_prime);
	int c = ath_prime * bth_prime;
	printf("c: %i\n", c);
	int m = (ath_prime - 1) * (bth_prime - 1);
	printf("m: %i\n", m);
	int e = RSA_composite_coprime(m, c);
	printf("e: %i\n", e);
	int d = modular_inverse(e, m);
	printf("d: %i\n", d);
	int **keys = malloc(sizeof(int*)*2);
	// Public key
	keys[0] = malloc(sizeof(int)*2);
	keys[0][0] = e;
	keys[0][1] = c;
	// Private key
	keys[1] = malloc(sizeof(int)*2);
	keys[1][0] = d;
	keys[1][1] = c;
	return keys;
}

