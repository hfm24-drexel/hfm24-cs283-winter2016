#ifndef _RSA_H_
#define _RSA_H_
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// Returns 1 if n is prime, 0 if it isn't
int is_prime(int n); 

// Uses Euclid's method to determine if the greatest common denominator of both numbers is 1
int is_coprime(int m, int n);

// Finds the nth prime number in the list of all prime numbers starting at 0.
int nth_prime(int n);
 
// Specifically for use in RSA encryption. Finds a composite number x that is coprime to both m and c
int RSA_composite_coprime(int m, int c);
 
// Finds the modular inverse of m mod n
int modular_inverse(int m, int n);

// Encrypts an ascii-character cipher using key and c
int endecrypt(int cipher, int key, int c);
 
// Generates a public and private key in a 2d array of integers, keys. Keys[0][0] is the public key e, keys[0][1] is the public key c, keys[1][0] is the private key d, and 
// keys[1][1] is the private key c (same as public key c)
int **RSA_gen_keys(int a, int b);

#endif
