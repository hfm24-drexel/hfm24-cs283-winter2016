#include "Dynamic_Array.h"
#include <stdlib.h>

struct Dynamic_Array* initArray(int array_Size) {
	// initialize Dynamic array object with enough space for the array and the size of an element
	struct Dynamic_Array* dynArray = (struct Dynamic_Array*) malloc(sizeof(struct Dynamic_Array));
	dynArray->array = malloc(array_Size*sizeof(int));
	dynArray->arraySize = array_Size;
	dynArray->currentIndex = 0;
	int i;
	for (i = 0; i < array_Size; i++) {
		dynArray->array[i] = (int)NULL;
	}
	return dynArray;
}	

struct Dynamic_Array* arrayAdd(struct Dynamic_Array* dynArray, int element) {
	int val = dynArray->array[0];
	// If no space is available, create space for one more element
	if (dynArray->currentIndex >= dynArray->arraySize) {
		int* newArray = realloc(dynArray->array, (dynArray->arraySize+1)*sizeof(int));
		dynArray->array = newArray;
		dynArray->arraySize = dynArray->arraySize+1;
	}
	dynArray->array[dynArray->currentIndex] = element;
	dynArray->currentIndex++;
	return dynArray;
}

struct Dynamic_Array* arrayExpAdd(struct Dynamic_Array* dynArray, int element) {
	int val = dynArray->array[0];
	// If no space is available, create space for twice as many elements as the current size of the array
	if (dynArray->currentIndex >= dynArray->arraySize) {
		int* newArray = realloc(dynArray->array, (dynArray->arraySize*2)*sizeof(int));
		dynArray->array = newArray;
		dynArray->arraySize = dynArray->arraySize*2;
	}
	dynArray->array[dynArray->currentIndex] = element;
	dynArray->currentIndex++;
	return dynArray;

}

struct Dynamic_Array* arrayRemove(struct Dynamic_Array* dynArray) {
	if (dynArray->currentIndex == 0) {
		// No elements left to remove, so decrease the size of the array by 1
		if (dynArray->arraySize == 0)
			return dynArray;
		int* newArray = realloc(dynArray->array, (dynArray->arraySize-1)*sizeof(int));
		dynArray->arraySize--;
		dynArray->array = newArray;
	} else {
		// decrement the current index by 1, and remove that element
		dynArray->currentIndex--; 
		dynArray->array[dynArray->currentIndex] = (int)NULL;
	}
	return dynArray;
}

int arrayGet(struct Dynamic_Array* dynArray, int index) {
	return dynArray->array[index];
}
