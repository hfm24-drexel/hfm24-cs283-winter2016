#ifndef DYNAMIC_ARRAY
#define DYNAMIC_ARRAY


struct Dynamic_Array {
	int* array; // The array itself
	int arraySize; // number of indices in the array
	int currentIndex;
};

// Initializes array with number of indices and the size of each element
struct Dynamic_Array* initArray(int array_Size);

// Adds a new element to the next free spot in the array. If no space is available, memory is reallocated to allow for 1 more element beforehand.
struct Dynamic_Array* arrayAdd(struct Dynamic_Array* dynArray, int element);

// Adds a new element to the next free spot in the array. If no space is available, memory is reallocated to allow for twice as many elements in the array beforehand.
struct Dynamic_Array* arrayExpAdd(struct Dynamic_Array* dynArray, int element);

// Removes the last element from the array
struct Dynamic_Array* arrayRemove(struct Dynamic_Array* dynArray);

// Returns the element in the input index
int arrayGet(struct Dynamic_Array* dynArray, int index);

#endif
