Hayden McPhee
CS 283: Systems Architecture
Lab 1

Programming Environment: Drexel Tux server
Operating System/Computer: Windows 10 locally

Requirements: makefile, Dynamic_Array.h, Dynamic_Array.c, and mcphee_L1.c (all should be included)

To run, simply type 'make' and hit enter, and the results should be printed out to console

General Description: This is Lab 1 for CS 283, and involves practice with the C programming language. Included are 5 problems:
	- The first problem is practice in building an array using pointers
	- The second demonstrates creating an array of character arrays
	- The third sorts an array using pointer arithmetic
	- The fourth sorts a linked list struct
	- The fifth implements a dynamic array, which can reallocate memory as needed if more elements are added than its current size.
