#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Dynamic_Array.h"

// Creates an array of integer pointers and prints them out, then frees the space in memory.
void prob1() {
	printf("\nProblem 1:\n\n");
	int X_LENGTH = sizeof(int)*10;
	int* x = (int*) malloc(X_LENGTH);
	int i;

	for (i = 0; i < X_LENGTH/sizeof(int); i++) {
		x[i] = i;
		printf("%d\n", x[i]);
	}
	free(x);
	printf("\n\n");
}

// Creates an array of character arrays
void prob2() {
	printf("\nProblem 2:\n\n");
	int X_LENGTH = sizeof(char*)*10;
	char** x = (char**) malloc(X_LENGTH);
	int i;
	for (i = 0; i < X_LENGTH/sizeof(char*); i++) {
		x[i] = (char*) malloc(sizeof(char)*15);
		strcpy(x[i], "Hello, World!\0");
		printf("%s\n", x[i]);
	}
	free(x);
	printf("\n\n");
}

// Helper for partition
void swap(int* array, int i1, int i2) {
		int tmp = array[i1];
	array[i1] = array[i2];
	array[i2] = tmp;
}

// Helper for quicksort
int partition(int* array, int hi, int lo) {
	int pivot = array[(hi+lo)/2];
	int pivotIdx  = (hi+lo)/2;
	int i = lo;
	int j = hi;
	while (i <= j) {
		while (array[i] < pivot) {
			i++;
		}
		while (array[j] > pivot) {
			j--;
		}
		if (i <= j) {
			swap(array, i, j);
			i++;
			j--;
		}
	}
	return i;
}

void printArray(int* array, int size) {
	int i;
	for (i = 0; i < size; i++) {
		printf("%d ", array[i]);
	}
	printf("\n");
}

void quicksort(int* array, int hi, int lo) {
	int p = partition(array, hi, lo);	
	if (lo < p-1)
		quicksort(array, p-1, lo);
	if (p < hi)
		quicksort(array, hi, p);
}

// Handles testing of quicksort, which uses pointer arithmetic
void prob3() {
	printf("\nProblem 3:\n\n");
	// Constructing array
	int* prob3Array = (int*) malloc(sizeof(int)*6);
	prob3Array[0] = 5;
	prob3Array[1] = 2;
	prob3Array[2] = 3;
	prob3Array[3] = 9;
	prob3Array[4] = 1;
	prob3Array[5] = 25;

	// Sorting array
	printf("Sorting Array {5 2 3 9 1 25}...\n");
	quicksort(prob3Array, 5, 0);
	printArray(prob3Array, 6);	
	printf("\n\n");
}

struct listNode {
	int value;
	struct listNode* next;
};

void printList(struct listNode* root) {
	struct listNode* node = root;
	while (node != NULL) {
		printf("%d ", node->value);
		node = node->next;
	}
	printf("\n");
}

// Simple sorter for a linked list. Part of prob4
void listSort(struct listNode* root) {
	struct listNode* counter = root;
	int count = 0;
	while (counter != NULL) {
		count++;
		counter = counter->next;
	}

	int i;
	for (i = 0; i < count; i++) {
		struct listNode* current = root;
		struct listNode* trail = root; 
		while (current->next != NULL) {
			// If the next value should be before the current one, swap the two. 
			if (current->next->value < current->value && current != root) {
				struct listNode* tmp = current->next;
				current->next = current->next->next;
				tmp->next = current;
				trail->next = tmp;
				current = tmp;
			}
			trail = current;
			current = current->next;
		}	
	}
}


// Handles creation of a test linked list and testing of listSorter
void prob4() {
	struct listNode* node5;
	struct listNode* node4;
	struct listNode* node3;
	struct listNode* node2;
	struct listNode* node1;
	struct listNode* node0;
	struct listNode* root;

	node5 = (struct listNode*) malloc(sizeof(struct listNode));
	node4 = (struct listNode*) malloc(sizeof(struct listNode));
	node3 = (struct listNode*) malloc(sizeof(struct listNode));
	node2 = (struct listNode*) malloc(sizeof(struct listNode));
	node1 = (struct listNode*) malloc(sizeof(struct listNode));
	node0 = (struct listNode*) malloc(sizeof(struct listNode));
	root = (struct listNode*) malloc(sizeof(struct listNode));

	node5->value = 25;
	node4->value = 102;
	node3->value = 2;
	node2->value = 179;
	node1->value = -12;
	node0->value = 0;
	root->value = 0;

	node5->next = NULL;
	node4->next = node5;
	node3->next = node4;
	node2->next = node3;
	node1->next = node2;
	node0->next = node1;
	root->next = node0;

	printf("\nProblem 4: \n\n");
	printf("Sorting list:\t");
	printList(root);
	listSort(root);
	printList(root);
	printf("\n\n");
}

// Handles creation and testing of Dynamic Array
void prob5() {
	printf("\nProblem 5:\n\n");
	int ARR_SIZE = 10;
	struct Dynamic_Array* dynArray = initArray(ARR_SIZE);
	printf("Created new list of size %d: \n", ARR_SIZE);
	printArray(dynArray->array, dynArray->arraySize);
	arrayRemove(dynArray);
	printf("called arrayRemove() once: \n");
	printArray(dynArray->array, dynArray->arraySize);
	printf("adding integer '3':\n");
	arrayAdd(dynArray, 3);
	printArray(dynArray->array, dynArray->arraySize);
	printf("removing one more time:\n");
	arrayRemove(dynArray);
	printArray(dynArray->array, dynArray->arraySize);

	int i;
	printf("Adding 100000 elements using simple add...\n");
	clock_t begin, end;
	double time_spent;
	begin = clock();
	for (i = 0; i < 100000; i++) {
		arrayAdd(dynArray, 3);
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Adding took %G seconds\n\n", time_spent);

	printf("Adding 100000 elements using exponential add...\n");
	begin = clock();
	for (i = 0; i < 100000; i++) {
		arrayExpAdd(dynArray, 3);
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Addint gook %G seconds\n\n", time_spent);	
}

int main(int argc, char** argv) {
	prob1();
	prob2();
	prob3();
	prob4();
	prob5();

	return 0;
}
