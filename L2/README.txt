Hayden McPhee
CS283: Systems Architecture 1
Lab 2 (L2)
email: hfm24@drexel.edu

Files that should be present:
	makefile
	mcphee_L2.c
	csapp.h
	csapp.c

Description: This program syncs two directories. If a file or directory exists in the second directory but not the first, it is deleted. If a file or directory exists in the first directory, but not the second, it is copied into the second. If a file exists in both directories, the one with the most recent modified date is copied from one directory to the other. If a directory exists in both directories, the two are compared in the same manner.

To run:
use "make sync" and "make clean" to run and clean the program.
The program takes in two arguments: the paths of both directories. Therefore, the make command requires two variables DIR1 and DIR2, so that running "make sync" should look like "DIR1=[path] DIR2=[path] make sync" (the same as is shown in the lab description).

Additionally, a make command is included to build two test directories: "make build-dirs". 
