#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include "csapp.h"

// Checks for the first occurence of val in arr. Returns the index of the array where it was found if it was found, and returns -1 otherwise.
int contains(char** arr, int arrSize, char* val) {
	int i;
	for (i = 0; i < arrSize; i++) {
		if ((strcmp(arr[i], val)) == 0) {
			return i;
		}
	}
	return -1;
}

// Copies the file found in the src path into the file in the dst path. Creates a destination file if it doesn't already exist.
int copyFile(char* src, char* dst) {
	char buf[1024];
	int fd1, fd2, n;

	// Open the file to be copied
	if ((fd1 = open(src, O_RDONLY, 0)) == -1) {
		return -1;
	}

	// Open the file to be copied to. If it doesn't exist, create it.
	if ((fd2 = open(dst, O_WRONLY, 0)) == -1) {
		if ((fd2 = creat(dst, 0666)) == -1) {
			return -1;
		}
	}

	// Copy the source into the destination
	while ((n = read(fd1, buf, 1024)) > 0) {
		write(fd2, buf, n);
	}

	if ((close(fd1)) < 0) {
		fprintf(stderr, "Unable to close file: %s\n", src);
		exit(1);
	}

	if ((close(fd2)) < 0) {
		fprintf(stderr, "Unable to close file: %s\n", dst);
		exit(1);
	}

	return 0;
}

// Copies the contents of the source directory to the destination directory. If the destination directory doesn't exist, it is created before copying
int copyDir(char* src, char* dst) {
	DIR *directory = opendir(src);
	DIR *dstDirectory = opendir(dst);
	struct dirent *de, *de2;
	int retVal = 0;

	if (!directory) {
		return -1;
	}

	if (!dstDirectory) {
		if (mkdir(dst, 0700) == -1) {
			return -1;
		}
		dstDirectory = opendir(dst);
		if (!dstDirectory) {
			return -1;
		}
	}

	while (0 != (de = readdir(directory))) {
		struct stat fileStat;
		char *fileName = de->d_name;

		// Ignore the "." and ".." files
		if (strcmp(fileName, ".") == 0 || strcmp(fileName, "..") == 0) {
			continue;
		}

		char *filePath = malloc(strlen(src) + strlen("/") + strlen(fileName));
		strcpy(filePath, src);
		strcat(strcat(filePath, "/"), fileName);

		char *dstPath = malloc(strlen(dst) + strlen("/") + strlen(fileName));
		strcpy(dstPath, dst);
		strcat(strcat(dstPath, "/"), fileName);

		Stat(filePath, &fileStat);
		if (S_ISREG(fileStat.st_mode)) {
			retVal = copyFile(filePath, dstPath);
		} else {
			retVal = copyDir(filePath, dstPath);
		}
	}

	if (closedir(directory) < 0) {
		fprintf(stderr, "Unable to close directory: %s", src);
		exit(1);
	}

	if (closedir(dstDirectory) < 0) {
		fprintf(stderr, "Unable to close directory: %s", dst);
		exit(1);
	}

	return retVal;

}


int delDir(char* path) {
	DIR *directory = opendir(path);
	struct dirent *de;
	int retVal = 0;

	if (!directory) {
		return -1;
	}

	// Delete all of the directory's contents
	while (0 != (de = readdir(directory))) {
		struct stat fileStat;
		char *fileName = de->d_name;
		// Ignore the "." and ".." file names.
		if ((strcmp(fileName, ".")) == 0 || (strcmp(fileName, "..")) == 0) {
			continue;
		}

		char *filePath = malloc(strlen(path) + strlen("/") + strlen(fileName));
		strcpy(filePath, path);
		strcat(strcat(filePath, "/"), fileName); 		

		Stat(filePath, &fileStat);
		if (S_ISREG(fileStat.st_mode)) {
			retVal = delFile(filePath);
		} else {
			retVal = delDir(filePath);
		}
	}

	if (closedir(directory) < 0) {
		fprintf(stderr, "Unable to close directory: %s\n", path);
		exit(1);
	}

	// Delete the directory itself if there was no problem with deleting its contents
	if (retVal != -1) {
		retVal = rmdir(path);
	}	

	return retVal;
}


int delFile(char* path) {
	int retVal = 0;
	retVal = remove(path); 
	return retVal;
}


int compareDirs(char* dir1, char* dir2) {
	DIR *directory1, *directory2;
	struct dirent *de1, *de2;
	char **fileNames1 = malloc(sizeof(char*)*100);
	char **fileNames2 = malloc(sizeof(char*)*100);
	int dir1Size, dir2Size, retVal;

	// Try opening directories
	if (!(directory1 = opendir(dir1))) {
		error("Failed to open first directory");
	}
	if (!(directory2 = opendir(dir2))) {
		error("Failed to open second directory");
	}

	// Store directory contents in arrays
	int i = 0;
	while (0 != (de1 = readdir(directory1))) {
		fileNames1[i] = malloc(sizeof(de1->d_name));
		fileNames1[i] = de1->d_name;
		i++;
	}
	dir1Size = i;

	i = 0;
	while (0 != (de2 = readdir(directory2))) {
		fileNames2[i] = malloc(sizeof(de2->d_name));
		fileNames2[i] = de2->d_name;
		i++;
	}
	dir2Size = i;

	// iterate through second directory file names, deleting ones that don't show up in directory 1
	for (i = 0; i < dir2Size; i++) {
		char* fileName = fileNames2[i];
		if (strcmp(fileName, ".") == 0 || strcmp(fileName, "..") == 0) {
			continue;
		}
		if ((contains(fileNames1, dir1Size, fileName)) == -1) {
			struct stat fileStat;

			char* path = malloc(strlen(fileName) + strlen(dir2) + strlen("/"));
			strcpy(path, dir2);
			strcat(strcat(path, "/"), fileName);

			//printf("%s\n", path);
			Stat(path, &fileStat);
			if (S_ISREG(fileStat.st_mode)) {
				if (delFile(path) == -1) {
					printf("Unable to delete file: %s\n", path);
					retVal = -1;
				}
			} 
			else {
				if (delDir(path) == -1) {
					printf("Unable to delete directory: %s\n", path);
					retVal = -1;
				}
			}	
		}
	}
	

	// Now compare the first directory to the second, updating any shared files and copying files from directory 1 to directory 2 if they don't exist in directory 2
	for (i = 0; i < dir1Size; i++) {
		char* fileName1 = fileNames1[i];
		struct stat fileStat;

		// Ignore the "." and ".." files
		if (strcmp(fileName1, ".") == 0 || strcmp(fileName1, "..") == 0) {
			continue;
		}
			
		char* path = malloc(strlen(fileName1) + strlen(dir1) + strlen("/"));
		strcpy(path, dir1);
		strcat(strcat(path, "/"), fileName1);
		Stat(path, &fileStat);
		int contained = contains(fileNames2, dir2Size, fileName1);

		// If the second directory doesn't contain the file from directory 1, copy it over
		if (contained == -1) {
			char* path2 = malloc(strlen(fileName1) + strlen(dir2) + strlen("/"));
			strcpy(path2, dir2);
			strcat(strcat(path2, "/"), fileName1);
			if (S_ISREG(fileStat.st_mode)) {
				if (copyFile(path, path2) == -1) {
					printf("Unable to copy file:\n\tSource: %s\n\tDestination: %s\n", path, path2);
					retVal = -1;
				}
			} else {
				if (copyDir(path, path2) == -1) {
					printf("Unable to copy directory:\n\tSource: %s\n\tDestination: %s\n", path, path2);
					retVal = -1;	
				};
			}
		} else {
			// The file is in both directories, so get the path of the second file
			char* fileName2 = fileNames2[contained];
			char* path2 = malloc(strlen(fileName2) + strlen(dir2) + strlen("/"));
			strcpy(path2, dir2);
			strcat(strcat(path2, "/"), fileName2);
			
			// If it is a file and not a directory, copy the one with the most recent modified date from one directory to the other.
			if (S_ISREG(fileStat.st_mode)) {
				struct stat fileStat2;
			
				//printf("%s\n", path2);	
				Stat(path2, &fileStat2);
				// If the first file is older than the second one, replace the first one with the second one.
				if (fileStat2.st_mtime > fileStat.st_mtime) {
					if (copyFile(path2, path) == -1) {
						printf("Unable to copy file:\n\tSource: %s\n\tDestination: %s\n", path2, path);
						retVal = -1;
					}
				} else {
					// Otherwise, replace the second one with the first one
					if (copyFile(path, path2) == -1) {
						printf("Unable to copy file:\n\tSource: %s\n\tDestination: %s\n", path, path2);
						retVal = -1;
					}
				}
			} else {
				// If it is directory, compare the directories
				if (compareDirs(path, path2) == -1) {
					printf("Directory comparison failed:\n\tDirectory 1: %s\n\tDirectory 2: %s\n", path, path2);
					retVal = -1;
				}	
			}
		}	
	}
	closedir(directory1);
	closedir(directory2);

	return retVal;
	
}

void main(int argc, char** argv) {
	if (argc < 3) {
		printf("Please specify two directories to compare.\n");
		return;
	}

	char* dir1 = malloc(sizeof(argv[1]));
	dir1 = argv[1];
	char* dir2 = malloc(sizeof(argv[2]));
	dir2 = argv[2];
	int retVal = compareDirs(dir1, dir2);
	if (retVal == -1) {
		printf("Unsuccessfully synced directories.\n");
	} else {	
		printf("Successfully synced directories!\n");	
	}
}
