Hayden McPhee
CS283: Systems Architecture 1
Lab 3 (L3)
email: hfm24@drexel.edu

Files that should be present:
	Makefile
	mcphee_L3.c
	mcphee_L3_2.c
	csapp.h
	csapp.c

Description: This program is split into two parts. The first part sets up a client socket and sends a GET request to a host of your choosing. The second part sets up a server that receives GET requests for file paths and returns the contents of the files.
Part 1:
	Running: To run the first part, use "HOST=[Host name] PAGE=[Page name] make runClient". e.g. "HOST=www.google.com PAGE=/index.html make runClient".
	Description: This program, made from mcphee_L3.c, takes in two arguments: the host to connect to, and the page to request. It uses the host name to connect to a web server containing that name, and sends a GET request for the page specified, printing the response to the screen.

Part 2:
	Running: To run the second part, use "PORT=[Port number] make runServer". e.g. "PORT=15 make runServer".
	Description: This program, made from mcphee_L3_2.c, takes in one argument: the port to use when making the server. It creates a listening socket, then binds the socket to the specified port number. Then, it runs a server loop, where it calls accept() on any incoming connection. It then reads the connection's request, which should be of the format "GET /path HTTP/1.1\r\n\r\n", and uses the /path to open a file. It reads the contents of the file and responds witht the contents before closing the connection, and listens for the next connection.

Cleaning: to clean, simply use "make clean".
