#include "csapp.h"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

const int DEFAULT_PORT = 80;

void main(int argc, char** argv) {
	if (argc < 3) {
		printf("Please specify a web server to connect to as the first argument, and the page to get as the second argument.\n");
		return;
	}

	int clientfd;
	struct hostent *hp;
	struct sockaddr_in serveraddr;
	char* host = argv[1]; // Host to connect to
	char* page = argv[2]; // Page to get

	// Client socket creation
	printf("Creating client socket...\n");
	if ((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error: Unable to create a client socket. Exiting.\n");
		return;
	}
	printf("Client socket created successfully. Client fd: %i\n", clientfd);

	// Finding host
	printf("Getting host by name: %s\n", host);
	if ((hp = gethostbyname(host)) == NULL) {
		printf("Error: Unable to get the host. Exiting.\n");
		return;
	}
	printf("Host found successfully: %s \n", hp->h_name);

	// Filling in the structure
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char*) hp->h_addr_list[0], (char*) &serveraddr.sin_addr.s_addr, hp->h_length);
	serveraddr.sin_port = htons(DEFAULT_PORT);

	// Connecting to host
	printf("Connecting to server: %s\n", (char*) &serveraddr.sin_addr.s_addr);
	if (connect(clientfd, (SA*) &serveraddr, sizeof(serveraddr)) < 0) {
		printf("Error: Unable to connect to the server. Exiting.\n");
		return;
	}
	printf("Connected successfully.\n");


	// Filling in the GET message
	char *message = "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n";
	char messageLiteral [1024], messageReceived [4096];
	sprintf(messageLiteral, message, page, host);	

	// Sending the request
	printf("\nSending GET Request: \n%s\n", messageLiteral);
	int sent, received;
	if ((sent = send(clientfd, &messageLiteral, sizeof(messageLiteral), 0)) < 0) {
		printf("Error in sending GET request. Exiting.\n");
		return;
	}
	printf("Successfully sent GET request.\n");

	// Getting the response
	printf("Receiving Response.\n");
	if ((received = recv(clientfd, &messageReceived, sizeof(messageReceived), 0)) < 0) {
		printf("Error in receiving response. Exiting.\n");
		return;
	}
	printf("Successfully received response:\n%s\n", messageReceived);

	close(clientfd);
}
