#include "csapp.h"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>


void main(int argc, char** argv) {
	if (argc < 2) {
		printf("Please specify a port to use as the first argument\n");
		return;
	}

	int listenfd, optval = 1;
	struct sockaddr_in serveraddr;
	int port_no = strtoimax(argv[1], NULL, 10);
	char hostname[1024];
	hostname[1023] = "\0";
	gethostname(hostname, 1023);
	printf("Hostname: %s\n", hostname);	

	// Listening socket creation
	if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error: Unable to create socket. Exiting.\n");
		return;
	}

	// Eliminates "Address already in use" error from bind()
	if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *) &optval, sizeof(int)) < 0) {
		printf("Unable to set socket REUSEADDR option.\n");
		return;
	}

	// Fill in serveraddr specifications
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons((unsigned short) port_no);
	
	if (bind(listenfd, (SA*)&serveraddr, sizeof(serveraddr)) < 0) {
		printf("Unable to bind port to server socket.\n");
		return;
	}
	
	// Make server a listening one	
	if (listen(listenfd, LISTENQ) < 0) {
		printf("Unable to set server to a listening server.\n");
		return;
	}

	// Main server loop	
	int connfd;
	struct sockaddr_in clientaddr;
	int clientlen;
	struct hostent *hp;
	char *haddrp;
	size_t n;
	char request[1024];
	rio_t rio;
	
	printf("Running main server loop.\n");
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = accept(listenfd, (SA*) &clientaddr, &clientlen);
		hp = gethostbyaddr((const char*) &clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("\n\nserver connected to %s (%s)\n", hp->h_name, haddrp);

		n = read(connfd, request, sizeof(request));

		printf("Request: %s\n", request);
		// Get path inside the GET request		
		const char *pathBegin = strstr(request, "GET ")+4;
		const char *pathEnd = strstr(pathBegin, " HTTP");
		int pathLen = strlen(pathBegin) - strlen(pathEnd);
		char *path = (char*) malloc(sizeof(char)*(pathLen+1));
		strncpy(path, pathBegin, pathLen);
		strcpy(path+pathLen, "\0");
		printf("File to open: %s\n", path);

		// Open and read the file specified by the path
		int fd, m;
		char content[4096];
		if( (fd = open(path, O_RDONLY, 0)) < 0) {
			printf("Unable to open file: %s\n", path);
			close(connfd);
		}
		while ((m = read(fd, content, 4096)) > 0) {
			write(fd, content, m);
		}
		close(fd);

		// Return the contents of the file
		printf("Response:\n\t %s", content);
		n = write(connfd, content, strlen(content)+1);
		printf("Closing Connection\n");
		close(connfd);
	}
}
