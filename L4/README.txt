Hayden McPhee
CS283: Systems Architecture 1
Lab 4 (L4)
email: hfm24@drexel.edu

Files that should be present:
	makefile
	mcphee_L4_1.c
	mcphee_L4_2.c
	mcphee_L4_3.c

Description: The overall idea of the program is to measure the accuracy and speed of running pthreads that share a variable. In the program, 100 threads are used to each increment a shared variable n 1000 times. It is split into three versions:
	1. No mutex locking is used, so the resulting n after execution should be inaccurate
	2. Mutex locking is used inside the 1000x for loop. This takes much longer, but results in an accurate n
	3. Mutex locking is used outside the for loop. This takes about as much time as no mutex locking, and results in an accurate n

Running: use "make" to compile and run all three versions.

Cleaning: to clean, simply use "make clean". It should remove ver_1, ver_2, and ver_3 executables.
