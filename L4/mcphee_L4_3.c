#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *thread_increment(void* n) {
	int *n_int = (int*) n;
	int i;

	pthread_mutex_lock(&mutex);
	for (i = 0; i < 1000; i++) {
		(*n_int)++;
	}
	pthread_mutex_unlock(&mutex);
	pthread_exit(0);
}

void main(int argc, char** argv) {
	printf("\n\n\nRunning third version of program (mutex locking outside for loop)...\n");
	int run;
	float avg_time=0;
	for (run = 0; run < 10; run++) {
	 	clock_t begin, end;
		begin = clock();
		pthread_t threads[100];
		volatile int n = 0;
		int i;


		for (i = 0; i < 100; i++) {
			pthread_create(&threads[i], NULL, thread_increment, &n);
		}

		for (i = 0; i < 100; i++) {
			pthread_join(threads[i], NULL);
		}
		end = clock();
		float time = (float) (end - begin) / CLOCKS_PER_SEC;
		avg_time += time;
		printf("n after calling increment 100 times (should be 100,000): %d\n", n);
		printf("time elapsed: %f seconds\n", time);
	}
	pthread_mutex_destroy(&mutex);
	avg_time = avg_time / 10.0;

	printf("\nAverage time spent per program run: %f seconds\n", avg_time);
}
